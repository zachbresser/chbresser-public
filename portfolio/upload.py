import boto3, mimetypes, os
from google.cloud import storage
from io import BytesIO
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = 'misc_scripts/chbresser.json'

def handle_uploaded_file(f, static_path, upload_to_s3=False, upload_to_gcp=False):
    static_path = static_path + '/' if static_path[-1] != '/' else static_path

    if upload_to_s3:
        upload_file_to_s3(f, static_path)
    if upload_to_gcp:
        upload_file_to_gcp(f, static_path)

def upload_file_to_s3(f, static_path):
    s3 = boto3.resource('s3', 
              aws_access_key_id="", 
              aws_secret_access_key="")
    print("Putting file at {}".format(static_path + f.name))
    s3.Bucket('static.chbresser.com').put_object(Key=static_path + f.name, Body=f.read(), ContentType=get_mimetype(f), CacheControl='max-age=86400', ServerSideEncryption='AES256')

def upload_file_to_gcp(f, static_path):
    client = storage.Client('chbressertest')
    bucket = client.get_bucket('static.chbressertest.com')
    
    blob = bucket.blob(static_path + f.name)

    upload_file = BytesIO(f.read())
    blob.upload_from_file(upload_file, content_type=get_mimetype(f))
    print("Uploading {} to GCP".format(static_path + f.name))

def get_mimetype(f):
    return mimetypes.guess_type(f.name)[0] or 'application/octet-stream'
