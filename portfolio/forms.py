from django import forms
from portfolio.models import FileUpload

import datetime

class ContactForm(forms.Form):
    """ Initializes Form with Name, From Email, Subject, and Message, with classes for styling and placeholder values"""
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'formField formFieldHalf','placeholder': 'Name',									 'data-validation': 'alphanumeric','data-validation-allowing': '-_ '}),                                                          required=True)
    from_email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'formField formFieldHalf', 'placeholder': 'Your Email',
                                                                'data-validation': 'email'}),required=True)
    subject = forms.CharField(widget=forms.TextInput(attrs={'class': 'formField', 'placeholder': 'Subject',										'data-validation': 'length alphanumeric',											'data-validation-length': 'min3', 											'data-validation-allowing': ' -_()!?;:"\/{}'}),
                              required=True)
    message = forms.CharField(widget=forms.Textarea(attrs={'class': 'formField', 
	                                                       'placeholder': 'Message',
														   'data-validation': 'length alphanumeric',
       													   'data-validation-length': 'min3',
														   'data-validation-allowing': ' -_()!?;:"\/{}'}),
                              required=True)

class UploadForm(forms.ModelForm):

    upload_date = forms.DateField(required=True, disabled=True, initial=datetime.datetime.now())

    class Meta:
        model = FileUpload
        fields = ['description', 'upload_date', 'upload_to_s3', 'upload_to_gcp', 'static_path']

class FileUploadForm(UploadForm):
    
    uploaded_file = forms.FileField(required=True)

    class Meta(UploadForm.Meta):
        fields = UploadForm.Meta.fields + ['uploaded_file'] 
