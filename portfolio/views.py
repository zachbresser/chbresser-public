from django.contrib.auth.decorators import login_required, permission_required
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib import messages

from .forms import ContactForm, FileUploadForm
from .models import Project, Email
from chbresser.settings import EMAIL_HOST_USER, EMAIL_HOST_PASS
from .upload import handle_uploaded_file


def index(request):
    """ Return landing page with contact form """

    if request.method == 'GET':
        # Init a blank form
        form = ContactForm()
    else:
        # Grab data from POST, validate, and attempt to send mail.
        form = ContactForm(request.POST)
        if form.is_valid():
            name  = form.cleaned_data['name']
            from_email = form.cleaned_data['from_email']
            subject = 'contact chbresser.com from {}'.format(from_email)
            message = form.cleaned_data['message']
            try:
                send_mail(subject, message, from_email, ['admin@chbresser.com'],
                          auth_user=EMAIL_HOST_USER, auth_password=EMAIL_HOST_PASS)
                Email.objects.create(from_name=name, from_email=from_email, subject=subject, message=message)

            except BadHeaderError:
                # Fail on invalid header.
                return HttpResponse('Invalid Header found.')
            messages.success(request, 'Your email was sent successfully!')
            return redirect('/')
        # return blank form
    return render(request, 'index.html', {'form': form})

@permission_required('portfolio.add_fileupload')
def upload(request):
    """ Return File upload page """
    if request.method == 'GET':
        # Init a blank form
        form = FileUploadForm(initial={'upload_to_s3': True, 'upload_to_gcp': True})
    else:
        form = FileUploadForm(
               request.POST or None,
               request.FILES or None)
        # Grab data from POST, validate, and attempt to send mail
        if form.is_valid():
            description   = form.cleaned_data['description']
            upload_date   = form.cleaned_data['upload_date']
            upload_to_s3  = form.cleaned_data['upload_to_s3']
            upload_to_gcp = form.cleaned_data['upload_to_gcp']
            static_path   = form.cleaned_data['static_path']
           
            if upload_to_s3 or upload_to_gcp: 
                handle_uploaded_file(request.FILES['uploaded_file'], static_path, upload_to_s3, upload_to_gcp)
            
                # Save everything except the file in the DB for records.
                form.save()

                messages.success(request, 'Your File was uploaded successfully!') 
            return redirect('/upload')
        # If blank or invalid render the template with an empty form
    return render(request, 'upload.html', {'form': form})

def resume(request):
    """ Return resume page """
    return render(request, 'resume.html')


def projects(request):
    """ Return project page """
    return render(request, 'projects.html', {'projects': Project.objects.all()})
