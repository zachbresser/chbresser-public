from django.db import models

class Project(models.Model):
    
    identifier   = models.CharField(max_length=30)
    description  = models.TextField()
    profile_pic  = models.CharField(max_length=100)
    project_path = models.CharField(max_length=100)

    class Meta:
        managed = True
        db_table = 'projects'
        verbose_name_plural = 'projects'

    def __str__(self):
        return self.identifier

class Email(models.Model):
    
    from_name  = models.CharField(max_length=50)
    from_email = models.CharField(max_length=50)
    subject    = models.CharField(max_length=50)
    message    = models.TextField()
   
    class Meta:
        managed = True
        db_table = 'emails'
        verbose_name_plural = 'emails'

    def __str__(self):
        return "{}: {}".format(self.from_email, self.subject)

class FileUpload(models.Model):
    
    description   = models.TextField()
    upload_date   = models.DateField()
    upload_to_s3  = models.BooleanField()
    upload_to_gcp = models.BooleanField()
    static_path   = models.CharField(max_length=256)

    class Meta:
        managed = True
        db_table = 'uploads'
        verbose_name_plural = 'File Uploads'

    def __str__(self):
        return "{} uploaded on: {}".format(self.description, str(self.upload_date))
