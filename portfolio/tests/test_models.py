from django.test import TestCase
from django.urls import reverse

from portfolio.models import Project, Email

class EmailModelTest(TestCase):
    
    def setUp(self):
        Email.objects.create(from_email='test@chbressertest.com', from_name='test from django test',
                            subject='this is a test from django test', message='this is a test from django test')
    def test_from_name_max_length(self):
        email = Email.objects.get(pk=1)
        max_length = email._meta.get_field('from_name').max_length
        self.assertEquals(max_length, 50)
  
    def test_from_email_max_length(self):
        email = Email.objects.get(pk=1)
        max_length = email._meta.get_field('from_email').max_length
        self.assertEquals(max_length, 50)

    def test_subject_max_length(self):
        email = Email.objects.get(pk=1)
        max_length = email._meta.get_field('subject').max_length
        self.assertEquals(max_length, 50)

    def test_str_is_from_email_subject(self):
        email = Email.objects.get(pk=1)
        self.assertEqual("{}: {}".format(email.from_email, email.subject), str(email))

class ProjectModelTest(TestCase):
    
    def setUp(self):
        Project.objects.create(identifier='test project', description='this is a test project from django test',
                               profile_pic='/portfolio/test_img.png', project_path='/projects')

    def test_identifier_max_length(self):
        project = Project.objects.get(pk=1)
        max_length = project._meta.get_field('identifier').max_length
        self.assertEquals(max_length, 30)

    def test_profile_pic_max_length(self):
        project = Project.objects.get(pk=1)
        max_length = project._meta.get_field('profile_pic').max_length
        self.assertEquals(max_length, 100)

    def test_project_path_max_length(self):
        project = Project.objects.get(pk=1)
        max_length = project._meta.get_field('project_path').max_length
        self.assertEquals(max_length, 100)


