from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User

from portfolio.forms import ContactForm

class TestViews(TestCase):
        def setUp(self):
            test_user1 = User.objects.create_user(username='testuser1', password='12345') 
            test_user1.save()
            
            test_admin1 = User.objects.create_superuser(username='testadmin1', password='12345', email='test@test.com')
            test_admin1.save()

        def test_index_200(self):
            response = self.client.get(reverse("portfolio:index"))
            self.assertEqual(response.status_code, 200)

        def test_projects_200(self):
            response = self.client.get(reverse("portfolio:projects"))
            self.assertEqual(response.status_code, 200)

        def test_resume_200(self):
            response = self.client.get(reverse("portfolio:resume"))
            self.assertEqual(response.status_code, 200)
      
        def test_redirect_upload_if_not_logged_in(self):
            response = self.client.get(reverse("portfolio:upload"))
            self.assertRedirects(response, '/accounts/login/?next=/upload')
    
        def test_logged_in_can_view_page(self):
            login = self.client.login(username='testadmin1', password='12345')
            response = self.client.get(reverse("portfolio:upload"))
            self.assertEqual(response.status_code, 200)

        def test_logged_in_cant_veiw_page(self):
            login = self.client.login(username='testuser1', password='12345')
            response = self.client.get(reverse("portfolio:upload"))
            self.assertRedirects(response, '/accounts/login/?next=/upload')

class TestForm(TestCase):
    def test_contact_form(self):
        form_data = {
            "name": "Test Name",
            "from_email": "test@test.com",
            "subject":    "Test from Django manage.py test",
            "message": "This is a test from Django" }
        form = ContactForm(form_data)
        self.assertTrue(form.is_valid())

    def test_invalid_contact_form(self):
        form_data = {
            "name": "",
            "from_email": "mp",
            "subject":  "",
            "message": "this should fail" }
        form = ContactForm(form_data)
        self.assertFalse(form.is_valid())
