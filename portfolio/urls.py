from django.urls import path

from . import views

app_name = 'portfolio'

urlpatterns = [

    path('', views.index, name='index'),
    path('resume/', views.resume, name='resume'),
    path('projects/', views.projects, name='projects'),
    path('upload', views.upload, name='upload'),

]
