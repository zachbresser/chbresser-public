from django.contrib import admin
from .models import Project, Email

# Define the Admin class for Abilities
class ProjectAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'identifier', 'description')
    ordering = ['id']

class EmailAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'from_email', 'subject', 'message' )
    ordering = ['id']

# Register the Admin Classes
admin.site.register(Project, ProjectAdmin)
admin.site.register(Email, EmailAdmin)
