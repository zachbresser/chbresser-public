from django.test import TestCase
from django.urls import reverse

from pokedex.models import Abilities, Items, Moves, Pokemon, Regions, Stats, Types, Generations, PokemonColors, PokemonShapes, PokemonSpecies, GrowthRates

class AbilitiesModelTest(TestCase):
    
    def setUp(self):
        # Setup object used by all test methods
        Abilities.objects.create(identifier='test_identity', is_main_series=True, generation_id=1)
    
    def test_identifier_label(self):
        ability = Abilities.objects.get(pk=1)
        field_label = ability._meta.get_field('identifier').verbose_name
        self.assertEquals(field_label, 'identifier')
     
    def test_identifier_max_length(self):
        ability = Abilities.objects.get(pk=1)
        max_length = ability._meta.get_field('identifier').max_length
        self.assertEquals(max_length, 79)

    def test_str_is_identifier(self):
        ability = Abilities.objects.get(pk=1)
        self.assertEqual(ability.identifier, str(ability))

class ItemsModelTest(TestCase):
    
    def setUp(self):
        Items.objects.create(identifier='test_item', cost='1000', category_id=1)

    def test_identifier_label(self):
        item = Items.objects.get(pk=1)
        field_label = item._meta.get_field('identifier').verbose_name
        self.assertEquals(field_label, 'identifier')
  
    def test_identifier_max_length(self):
        item = Items.objects.get(pk=1)
        max_length = item._meta.get_field('identifier').max_length
        self.assertEquals(max_length, 79)

    def test_cost_greater_0(self):
        item = Items.objects.get(pk=1)
        self.assertTrue(item.cost >= 0)

    def test_str_is_identifier(self):
        item = Items.objects.get(pk=1)
        self.assertEqual(item.identifier, str(item))

class MovesModelTest(TestCase):
   
    def setUp(self):
        Generations.objects.create(identifier='test_generation', main_region_id=1)
        Types.objects.create(identifier='normal', generation=Generations.objects.get(id=1))
        Moves.objects.create(identifier='test_identity', type=Types.objects.get(id=1), power='40',
                             pp='35', accuracy='100', priority='0', damage_class_id='1', effect_id=1, 
                             generation_id=1, target_id=1)

    def test_identifier_label(self):
        move = Moves.objects.get(pk=1)
        field_label = move._meta.get_field('identifier').verbose_name
        self.assertEquals(field_label, 'identifier')

    def test_identifier_max_length(self):
        move = Moves.objects.get(pk=1)
        max_length = move._meta.get_field('identifier').max_length
        self.assertEquals(max_length, 79)

    def test_power_greater_than_0(self):
        move = Moves.objects.get(pk=1)
        self.assertTrue(move.power >= 0)

    def test_pp_greater_than_0(self):
        move = Moves.objects.get(pk=1)
        self.assertTrue(move.pp >= 0)

    def test_accuracy_greater_than_0(self):
        move = Moves.objects.get(pk=1)
        self.assertTrue(move.accuracy >= 0)
    
    def test_priority_greater_than_0(self):
        move = Moves.objects.get(pk=1)
        self.assertTrue(move.priority >= 0)

class PokemonModelTest(TestCase):
    def setUp(self):
        PokemonColors.objects.create(identifier='test_color')
        PokemonShapes.objects.create(identifier='test_shape')
        GrowthRates.objects.create(identifier='test_gr', formula='test_formula')
        PokemonSpecies.objects.create(identifier='test_species', color=PokemonColors.objects.get(id=1), 
                                      shape=PokemonShapes.objects.get(id=1), gender_rate=1, capture_rate=1,
                                      base_happiness=1, is_baby=False, hatch_counter=1, has_gender_differences=False,
                                      growth_rate=GrowthRates.objects.get(id=1), forms_switchable=True, order=1)
        Pokemon.objects.create(identifier='bulbasaur', species=PokemonSpecies.objects.get(id=1), 
                               height=7, weight=69, base_experience=64, order=1, is_default=False)

    def test_identifier_label(self):
        poke = Pokemon.objects.get(id=1)
        field_label = poke._meta.get_field('identifier').verbose_name
        self.assertEqual(field_label, 'identifier')
 
    def test_identifier_max_length(self):
        pokemon = Pokemon.objects.get(pk=1)
        max_length = pokemon._meta.get_field('identifier').max_length
        self.assertEquals(max_length, 79)

    def test_height_greater_than_0(self):
        poke = Pokemon.objects.get(id=1)
        self.assertTrue(poke.height >= 0)

    def test_weight_greater_than_0(self):
        poke = Pokemon.objects.get(id=1)
        self.assertTrue(poke.weight >= 0)

    def test_base_experience_greater_than_0(self):
        poke = Pokemon.objects.get(id=1)
        self.assertTrue(poke.base_experience >= 0)

    def test_str_method(self):
        poke = Pokemon.objects.get(id=1)
        self.assertEqual(str(poke), "#{}: {}".format(poke.id, poke.identifier))

class RegionsModelTest(TestCase):
     def setUp(self):
         Regions.objects.create(identifier='test_region')
 
     def test_identifier_label(self):
         region = Regions.objects.get(id=1)
         field_label = region._meta.get_field('identifier').verbose_name
         self.assertEqual(field_label, 'identifier')

     def test_str_method(self):
         region = Regions.objects.get(id=1)
         self.assertEqual(str(region), region.identifier)

class StatsModelTest(TestCase):
    def setUp(self):
       Stats.objects.create(identifier='test_stat', is_battle_only=False)

    def test_identifier_label(self):
        stat = Stats.objects.get(id=1)
        field_label = stat._meta.get_field('identifier').verbose_name
        self.assertEqual(field_label, 'identifier')

    def test_identifier_max_length(self):
        stat = Stats.objects.get(pk=1)
        max_length = stat._meta.get_field('identifier').max_length
        self.assertEquals(max_length, 79)

    def test_str_method(self):
        stat = Stats.objects.get(id=1)
        self.assertEqual(str(stat), stat.identifier)

class TypesModelTest(TestCase):
    def setUp(self):
       Generations.objects.create(identifier='test_generation', main_region_id=1)
       Types.objects.create(identifier='normal', generation=Generations.objects.get(id=1))

    def test_identifier_label(self):
        type = Types.objects.get(id=1)
        field_label = type._meta.get_field('identifier').verbose_name
        self.assertEqual(field_label, 'identifier')

    def test_identifier_max_length(self):
        type = Types.objects.get(pk=1)
        max_length = type._meta.get_field('identifier').max_length
        self.assertEquals(max_length, 79)

    def test_str_method(self):
        type = Types.objects.get(id=1)
        self.assertEqual(str(type), type.identifier)
