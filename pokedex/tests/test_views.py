from django.test import TestCase
from django.urls import reverse
from pokedex.models import PokemonColors, PokemonShapes, GrowthRates, PokemonSpecies, Pokemon, PokemonGenderRatios, GenderRatios

import random

# Create your tests here.
class TestViews(TestCase):
        def setUp(self):
            PokemonColors.objects.create(identifier='test_color')
            PokemonShapes.objects.create(identifier='test_shape')
            GrowthRates.objects.create(identifier='test_gr', formula='test_formula')
            PokemonSpecies.objects.create(identifier='test_species', color=PokemonColors.objects.get(id=1),
                                      shape=PokemonShapes.objects.get(id=1), gender_rate=1, capture_rate=1,
                                      base_happiness=1, is_baby=False, hatch_counter=1, has_gender_differences=False,
                                      growth_rate=GrowthRates.objects.get(id=1), forms_switchable=True, order=1)
            Pokemon.objects.create(identifier='bulbasaur', species=PokemonSpecies.objects.get(id=1),
                               height=7, weight=69, base_experience=64, order=1, is_default=False)
            GenderRatios.objects.create(percent_male='50', percent_female='50')
            PokemonGenderRatios.objects.create(pokemon=Pokemon.objects.get(id=1), ratio=GenderRatios.objects.get(id=1))


        def test_index_200(self):
            response = self.client.get(reverse("pokedex:index"))
            self.assertEqual(response.status_code, 200)

        def test_about_200(self):
            response = self.client.get(reverse("pokedex:about"))
            self.assertEqual(response.status_code, 200)

        def test_pokemon_detail_200(self):
            response = self.client.get(reverse("pokedex:pokemon", args=[1]))
            self.assertEqual(response.status_code, 200)

        def test_pokemon_detail_404(self):
            response = self.client.get(reverse("pokedex:pokemon", args=[1000]))
            self.assertEqual(response.status_code, 404)
        
        def test_pokemon_list_200(self):
            for i in range(1,10):
                response = self.client.get(reverse("pokedex:pokemon_list") + "?page={}".format(i))
                self.assertEqual(response.status_code, 200)
       
        def test_pokemon_list_404(self):
            response = self.client.get(reverse("pokedex:pokemon_list") + "?page=10")
            self.assertEqual(response.status_code, 404)
        
        def test_item_list_200(self):
            response = self.client.get(reverse("pokedex:item_list"))
            self.assertEqual(response.status_code, 200)
 
        def test_pokemon_category_list_200(self):
            response = self.client.get(reverse("pokedex:pokemon_category_list", args=['grass']))
            self.assertEqual(response.status_code, 200)

        def test_pokemon_category_list_404(self):
            response = self.client.get(reverse("pokedex:pokemon_category_list", args=['invalid']))
            self.assertEqual(response.status_code, 404)
