from django.shortcuts import render
from django.http import HttpResponseNotFound, HttpResponse

from .pokemon import Pokemon, PokemonList
from .item    import Item
from pokedex.models import Pokemon as PokemonModel

def index(request):
    return render(request, 'pokedex/pokedex.html')


def pokemon(request, id):
    if int(id) > 721:
        return HttpResponseNotFound('<h1 style="text-align: center">Sorry, you have went to an invalid page. :(</h1>'
                            '<h1 style="text-align: center">If this was in error, contact admin@chbresser.com</h1>')
    poke = Pokemon(id)
    context = {'pokemon': poke}

    return render(request, 'pokedex/pokemon.html', context)

def pokemon_list(request):
    page = request.GET.get('page', '1') # Get Page variable from request or use default on 1
    if int(page) > 9:
        return HttpResponseNotFound('<h1 style="text-align: center">Sorry you have went to an invalid page. :( </h1>'
                                    '<h1 style="text-align: center">You can go <a href="/pokedex/pokemon">here</a> for the Pokemon List</h1>')
    poke = PokemonList(page)
    context = {'poke': poke,
               'page': page }
    
    return render(request, 'pokedex/pokemon-list.html', context)    

def pokemon_category_list(request, poke_type):
    available_types = ['normal', 'fire', 'water', 'electric', 'grass', 'ice', 'fighting',
        'poison', 'ground', 'flying', 'psychic', 'bug', 'rock', 'ghost', 'dragon', 'dark',
        'steel', 'fairy']

    if poke_type in available_types:
        results = [ list(x) for x in list(PokemonModel.objects.select_related('pokemontypes_pokemon').filter(pokemontypes_pokemon__type__identifier=poke_type).values_list('id', 'identifier', 'height', 'weight')) ]
    else:
        return HttpResponseNotFound('<h1 style="text-align: center">Sorry you have went to an invalid page. :( </h1>'
                                    '<h1 style="text-align: center">You can go <a href="/pokedex/pokemon">here</a> for the Pokemon List </h1>')

    return render(request, 'pokedex/pokemon-category.html', { 'poke': results })

def item_list(request):
    item_list = Item.get_all()
    context   = { 'items': item_list }

    return render(request, 'pokedex/item-list.html', context)

def about(request):
    return render(request, 'pokedex/about.html')
