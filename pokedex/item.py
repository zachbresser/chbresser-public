import os, sqlite3

from pokedex.models import Items

class Item:
    """
        Class to represent a single Item
    """

    def __init__(self, item_id=0):
        if item_id != 0:
            results = Items.objects.get(id=item_id).select_related('category')
       
            self.id = item_id
            self.identifier = results.identifier.replace('-', ' ')
            self.category   = results.category.identifier.replace('-', ' ')

    def get_all():
        results = Items.objects.all().select_related('category')
        item_list = []
        for item in results:
            item_list.append([item.id, item.identifier.replace('-', ' '), item.category.identifier.replace('-', ' ')])

        return item_list
