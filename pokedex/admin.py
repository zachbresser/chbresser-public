from django.contrib import admin
from .models import Abilities, Items, Moves, Pokemon, Regions, Stats, Types

# Define the Admin class for Abilities
class AbilitiesAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'identifier')
    ordering = ['id']

class ItemsAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'identifier', 'category', 'cost')
    ordering = ['id']

class MovesAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'identifier', 'type')
    ordering = ['id']

class PokemonAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'identifier', 'height', 'weight')
    ordering = ['id']

class RegionsAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'identifier')
    ordering = ['id']

class StatsAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'identifier')
    ordering = ['id']

class TypesAdmin(admin.ModelAdmin):
    list_display = ( 'id', 'identifier')
    ordering = ['id']

# Register the Admin Classes
admin.site.register(Abilities, AbilitiesAdmin)
admin.site.register(Items, ItemsAdmin)
admin.site.register(Moves, MovesAdmin)
admin.site.register(Pokemon, PokemonAdmin)
admin.site.register(Regions, RegionsAdmin)
admin.site.register(Stats, StatsAdmin)
admin.site.register(Types, TypesAdmin)
