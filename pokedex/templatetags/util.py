from django import template

register = template.Library()

@register.filter
def get_type(value):
    if type(value) is list:
        return 'list'
    else:
        return type(value)

