# Generated by Django 2.0.6 on 2018-07-16 21:01

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('pokedex', '0002_auto_20180714_2041'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pokemontypes',
            name='pokemon',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='pokemontypes_pokemon', to='pokedex.Pokemon'),
        ),
    ]
