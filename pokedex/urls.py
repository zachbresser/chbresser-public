from django.urls import path

from . import views

app_name = 'pokedex'

urlpatterns = [

    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('pokemon/', views.pokemon_list, name='pokemon_list'),
    path('pokemon/<id>/', views.pokemon, name='pokemon'),
    path('pokemon/category/<poke_type>', views.pokemon_category_list, name='pokemon_category_list'),
    path('item/', views.item_list, name='item_list'),
]
