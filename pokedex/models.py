# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Abilities(models.Model):
    
    identifier = models.CharField(max_length=79)
    generation = models.ForeignKey('Generations', models.DO_NOTHING, related_name='ability_generation')
    is_main_series = models.BooleanField()

    class Meta:
        managed = True
        db_table = 'abilities'
        verbose_name_plural = 'abilities'
    
    def __str__(self):
        return self.identifier

class AbilityNames(models.Model):
    ability = models.ForeignKey(Abilities, models.DO_NOTHING, related_name='abilitynames_ability')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='abilitynames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'ability_names'
        unique_together = (('ability', 'local_language'),)


class AbilityProse(models.Model):
    ability = models.ForeignKey(Abilities, models.DO_NOTHING, related_name='abilityprose_ability')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='abilityprose_local_language')
    short_effect = models.TextField(blank=True, null=True)
    effect = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'ability_prose'
        unique_together = (('ability', 'local_language'),)


class Berries(models.Model):
    
    item = models.ForeignKey('Items', models.DO_NOTHING, related_name='berries_item')
    firmness = models.ForeignKey('BerryFirmness', models.DO_NOTHING, related_name='berries_firmness')
    natural_gift_power = models.IntegerField(blank=True, null=True)
    natural_gift_type = models.ForeignKey('Types', models.DO_NOTHING, related_name='berries_natural_gift_type', blank=True, null=True)
    size = models.IntegerField()
    max_harvest = models.IntegerField()
    growth_time = models.IntegerField()
    soil_dryness = models.IntegerField()
    smoothness = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'berries'


class BerryFirmness(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'berry_firmness'


class BerryFirmnessNames(models.Model):
    berry_firmness = models.ForeignKey(BerryFirmness, models.DO_NOTHING, related_name='berryfirmnessnames_berry_firmness')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='berryfirmnessnames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'berry_firmness_names'
        unique_together = (('berry_firmness', 'local_language'),)


class BerryFlavors(models.Model):
    berry = models.ForeignKey(Berries, models.DO_NOTHING, related_name='berryflavors_berry')
    contest_type = models.ForeignKey('ContestTypes', models.DO_NOTHING, related_name='berryflavors_contest_type')
    flavor = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'berry_flavors'
        unique_together = (('berry', 'contest_type'),)


class CharacteristicText(models.Model):
    characteristic = models.ForeignKey('Characteristics', models.DO_NOTHING, related_name='characteristictext_characteristic')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='characteristictesxt_local_language')
    message = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'characteristic_text'
        unique_together = (('characteristic', 'local_language'),)


class Characteristics(models.Model):
    
    stat = models.ForeignKey('Stats', models.DO_NOTHING, related_name='characteristics_stat')
    gene_mod_5 = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'characteristics'


class ContestCombos(models.Model):
    first_move = models.ForeignKey('Moves', models.DO_NOTHING, related_name='contestcombos_first_move')
    second_move = models.ForeignKey('Moves', models.DO_NOTHING, related_name='contestcombos_second_move')

    class Meta:
        managed = True
        db_table = 'contest_combos'
        unique_together = (('first_move', 'second_move'),)


class ContestEffectProse(models.Model):
    contest_effect = models.ForeignKey('ContestEffects', models.DO_NOTHING, related_name='contesteffectprose_contest_effect')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='contesteffectprose_local_language')
    flavor_text = models.TextField(blank=True, null=True)
    effect = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'contest_effect_prose'
        unique_together = (('contest_effect', 'local_language'),)


class ContestEffects(models.Model):
    
    appeal = models.SmallIntegerField()
    jam = models.SmallIntegerField()

    class Meta:
        managed = True
        db_table = 'contest_effects'


class ContestTypeNames(models.Model):
    contest_type = models.ForeignKey('ContestTypes', models.DO_NOTHING, related_name='contesttypenames_contest_type')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='contesttypenames_local_language')
    name = models.CharField(max_length=79, blank=True, null=True)
    flavor = models.TextField(blank=True, null=True)
    color = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'contest_type_names'
        unique_together = (('contest_type', 'local_language'),)


class ContestTypes(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'contest_types'


class EggGroupProse(models.Model):
    egg_group = models.ForeignKey('EggGroups', models.DO_NOTHING, related_name='egggroupprose_egg_group')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='egggroupprose_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'egg_group_prose'
        unique_together = (('egg_group', 'local_language'),)


class EggGroups(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'egg_groups'


class EncounterConditionProse(models.Model):
    encounter_condition = models.ForeignKey('EncounterConditions', models.DO_NOTHING, related_name='encounterconditionprose_encounter_condition')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='encounterconditionprose_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'encounter_condition_prose'
        unique_together = (('encounter_condition', 'local_language'),)


class EncounterConditionValueMap(models.Model):
    encounter = models.ForeignKey('Encounters', models.DO_NOTHING, related_name='encounterconditionvaluemap_encounter')
    encounter_condition_value = models.ForeignKey('EncounterConditionValues', models.DO_NOTHING, related_name='encounterconditionvaluemap_encounter_condition_value')

    class Meta:
        managed = True
        db_table = 'encounter_condition_value_map'
        unique_together = (('encounter', 'encounter_condition_value'),)


class EncounterConditionValueProse(models.Model):
    encounter_condition_value = models.ForeignKey('EncounterConditionValues', models.DO_NOTHING, related_name='encounterconditionvalueprose_encounter_condition_value')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='encounterconditionvalueprose_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'encounter_condition_value_prose'
        unique_together = (('encounter_condition_value', 'local_language'),)


class EncounterConditionValues(models.Model):
    
    encounter_condition = models.ForeignKey('EncounterConditions', models.DO_NOTHING, related_name='encounterconditionvalues_encounter_condition')
    identifier = models.CharField(max_length=79)
    is_default = models.BooleanField()

    class Meta:
        managed = True
        db_table = 'encounter_condition_values'


class EncounterConditions(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'encounter_conditions'


class EncounterMethodProse(models.Model):
    encounter_method = models.ForeignKey('EncounterMethods', models.DO_NOTHING, related_name='encountermethodprose_encounter_method')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='encountermethodprose_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'encounter_method_prose'
        unique_together = (('encounter_method', 'local_language'),)


class EncounterMethods(models.Model):
    
    identifier = models.CharField(unique=True, max_length=79)
    order = models.IntegerField(unique=True)

    class Meta:
        managed = True
        db_table = 'encounter_methods'


class EncounterSlots(models.Model):
    
    version_group = models.ForeignKey('VersionGroups', models.DO_NOTHING, related_name='encounterslots_version_group')
    encounter_method = models.ForeignKey(EncounterMethods, models.DO_NOTHING, related_name='encounterslots_encounter_method')
    slot = models.IntegerField(blank=True, null=True)
    rarity = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'encounter_slots'


class Encounters(models.Model):
    
    version = models.ForeignKey('Versions', models.DO_NOTHING, related_name='encounters_version')
    location_area = models.ForeignKey('LocationAreas', models.DO_NOTHING, related_name='encounters_location_area')
    encounter_slot = models.ForeignKey(EncounterSlots, models.DO_NOTHING, related_name='encounters_encounter_slot')
    pokemon = models.ForeignKey('Pokemon', models.DO_NOTHING, related_name='encounters_pokemon')
    min_level = models.IntegerField()
    max_level = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'encounters'


class EvolutionChains(models.Model):
    
    baby_trigger_item = models.ForeignKey('Items', models.DO_NOTHING, related_name='evolutionchains_baby_trigger_item', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'evolution_chains'


class EvolutionTriggerProse(models.Model):
    evolution_trigger = models.ForeignKey('EvolutionTriggers', models.DO_NOTHING, related_name='evolutiontriggerprose_evolution_trigger')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='evolutiontriggerprose_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'evolution_trigger_prose'
        unique_together = (('evolution_trigger', 'local_language'),)


class EvolutionTriggers(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'evolution_triggers'


class Experience(models.Model):
    growth_rate = models.ForeignKey('GrowthRates', models.DO_NOTHING, related_name='experience_growth_rate')
    level = models.IntegerField()
    experience = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'experience'
        unique_together = (('growth_rate', 'level'),)


class GenderRatios(models.Model):
    
    percent_male = models.TextField(blank=True, null=True)
    percent_female = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'gender_ratios'


class Genders(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'genders'


class GenerationNames(models.Model):
    generation = models.ForeignKey('Generations', models.DO_NOTHING, related_name='generationnames_generation')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='generationnames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'generation_names'
        unique_together = (('generation', 'local_language'),)


class Generations(models.Model):
    
    main_region = models.ForeignKey('Regions', models.DO_NOTHING, related_name='generations_main_region')
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'generations'


class GrowthRateProse(models.Model):
    growth_rate = models.ForeignKey('GrowthRates', models.DO_NOTHING, related_name='growthrateprose_growth_rate')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='growthrateprose_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'growth_rate_prose'
        unique_together = (('growth_rate', 'local_language'),)


class GrowthRates(models.Model):
    
    identifier = models.CharField(max_length=79)
    formula = models.TextField()

    class Meta:
        managed = True
        db_table = 'growth_rates'


class ItemCategories(models.Model):
    
    pocket = models.ForeignKey('ItemPockets', models.DO_NOTHING, related_name='itemcategories_pocket')
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'item_categories'


class ItemCategoryProse(models.Model):
    item_category = models.ForeignKey(ItemCategories, models.DO_NOTHING, related_name='itemcategoryprose_item_category')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='itemcategoryprose_lcoal_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'item_category_prose'
        unique_together = (('item_category', 'local_language'),)


class ItemFlagMap(models.Model):
    item = models.ForeignKey('Items', models.DO_NOTHING, related_name='itemflagmap_item')
    item_flag = models.ForeignKey('ItemFlags', models.DO_NOTHING, related_name='itemflagmap_item_flag')

    class Meta:
        managed = True
        db_table = 'item_flag_map'
        unique_together = (('item', 'item_flag'),)


class ItemFlagProse(models.Model):
    item_flag = models.ForeignKey('ItemFlags', models.DO_NOTHING, related_name='itemflagprose_item_flag')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='itemflagprose_local_language')
    name = models.CharField(max_length=79, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'item_flag_prose'
        unique_together = (('item_flag', 'local_language'),)


class ItemFlags(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'item_flags'


class ItemFlavorSummaries(models.Model):
    item = models.ForeignKey('Items', models.DO_NOTHING, related_name='itemflavorsummaries_item')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='itemflavorsummaries_local_language')
    flavor_summary = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'item_flavor_summaries'
        unique_together = (('item', 'local_language'),)


class ItemFlavorText(models.Model):
    item = models.ForeignKey('Items', models.DO_NOTHING, related_name='itemflavortext_item')
    version_group = models.ForeignKey('VersionGroups', models.DO_NOTHING, related_name='itemflavortext_version_group')
    language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='itemflavortext_language')
    flavor_text = models.TextField()

    class Meta:
        managed = True
        db_table = 'item_flavor_text'
        unique_together = (('item', 'version_group', 'language'),)


class ItemFlingEffectProse(models.Model):
    item_fling_effect = models.ForeignKey('ItemFlingEffects', models.DO_NOTHING, related_name='itemflingeffectprose_item_fling_effect')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='itemflingeffectprose_local_language')
    effect = models.TextField()

    class Meta:
        managed = True
        db_table = 'item_fling_effect_prose'
        unique_together = (('item_fling_effect', 'local_language'),)


class ItemFlingEffects(models.Model):
    

    class Meta:
        managed = True
        db_table = 'item_fling_effects'


class ItemGameIndices(models.Model):
    item = models.ForeignKey('Items', models.DO_NOTHING, related_name='itemgameindices_item')
    generation = models.ForeignKey(Generations, models.DO_NOTHING, related_name='itemgameindices_generation')
    game_index = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'item_game_indices'
        unique_together = (('item', 'generation'),)


class ItemNames(models.Model):
    item = models.ForeignKey('Items', models.DO_NOTHING, related_name='itemnames_item')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='itemnames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'item_names'
        unique_together = (('item', 'local_language'),)


class ItemPocketNames(models.Model):
    item_pocket = models.ForeignKey('ItemPockets', models.DO_NOTHING, related_name='itempocketnames_item_pocket')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='itempocketnames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'item_pocket_names'
        unique_together = (('item_pocket', 'local_language'),)


class ItemPockets(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'item_pockets'


class ItemProse(models.Model):
    item = models.ForeignKey('Items', models.DO_NOTHING, related_name='itemprose_item')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='itemprose_local_language')
    short_effect = models.TextField(blank=True, null=True)
    effect = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'item_prose'
        unique_together = (('item', 'local_language'),)


class Items(models.Model):
    
    identifier = models.CharField(max_length=79)
    category = models.ForeignKey(ItemCategories, models.DO_NOTHING, related_name='items_category')
    cost = models.IntegerField()
    fling_power = models.IntegerField(blank=True, null=True)
    fling_effect = models.ForeignKey(ItemFlingEffects, models.DO_NOTHING, related_name='items_fling_effect', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'items'
        verbose_name_plural = 'items'
    
    def __str__(self):
        return self.identifier

class LanguageNames(models.Model):
    language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='languagenames_language')
    local_language = models.ForeignKey('Languages', models.DO_NOTHING, related_name='languagenames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'language_names'
        unique_together = (('language', 'local_language'),)


class Languages(models.Model):
    
    iso639 = models.CharField(max_length=79)
    iso3166 = models.CharField(max_length=79)
    identifier = models.CharField(max_length=79)
    official = models.BooleanField()
    order = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'languages'


class LocationAreaEncounterRates(models.Model):
    location_area = models.ForeignKey('LocationAreas', models.DO_NOTHING, related_name='locationareaencounterrates_location_area')
    encounter_method = models.ForeignKey(EncounterMethods, models.DO_NOTHING, related_name='locationareaencounterrates_encounter_method')
    version = models.ForeignKey('Versions', models.DO_NOTHING, related_name='locationareaencounterrates_version')
    rate = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'location_area_encounter_rates'
        unique_together = (('location_area', 'encounter_method', 'version'),)


class LocationAreaProse(models.Model):
    location_area = models.ForeignKey('LocationAreas', models.DO_NOTHING, related_name='locationareaprose_location_area')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='locationareaprose_local_language')
    name = models.CharField(max_length=79, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'location_area_prose'
        unique_together = (('location_area', 'local_language'),)


class LocationAreas(models.Model):
    
    location = models.ForeignKey('Locations', models.DO_NOTHING, related_name='locationareas_location')
    game_index = models.IntegerField()
    identifier = models.CharField(max_length=79, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'location_areas'


class LocationGameIndices(models.Model):
    location = models.ForeignKey('Locations', models.DO_NOTHING, related_name='locationgameindices_location')
    generation = models.ForeignKey(Generations, models.DO_NOTHING, related_name='locationgameindices_generation')
    game_index = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'location_game_indices'
        unique_together = (('location', 'generation', 'game_index'),)


class LocationNames(models.Model):
    location = models.ForeignKey('Locations', models.DO_NOTHING, related_name='locationnames_location')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='locationnames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'location_names'
        unique_together = (('location', 'local_language'),)


class Locations(models.Model):
    
    region = models.ForeignKey('Regions', models.DO_NOTHING, related_name='locations_region', blank=True, null=True)
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'locations'


class Machines(models.Model):
    machine_number = models.IntegerField()
    version_group = models.ForeignKey('VersionGroups', models.DO_NOTHING, related_name='machines_version_group')
    item = models.ForeignKey(Items, models.DO_NOTHING, related_name='machines_item')
    move = models.ForeignKey('Moves', models.DO_NOTHING, related_name='machiens_move')

    class Meta:
        managed = True
        db_table = 'machines'
        unique_together = (('machine_number', 'version_group'),)


class MoveBattleStyleProse(models.Model):
    move_battle_style = models.ForeignKey('MoveBattleStyles', models.DO_NOTHING, related_name='movebattlestyleprose_move_battle_style')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='movebattlestyleprose_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'move_battle_style_prose'
        unique_together = (('move_battle_style', 'local_language'),)


class MoveBattleStyles(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'move_battle_styles'


class MoveChangelog(models.Model):
    move = models.ForeignKey('Moves', models.DO_NOTHING, related_name='movechangelog_move')
    changed_in_version_group = models.ForeignKey('VersionGroups', models.DO_NOTHING, related_name='movechangelog_changed_in_version_group')
    type = models.ForeignKey('Types', models.DO_NOTHING, related_name='movechangelog_type', blank=True, null=True)
    power = models.SmallIntegerField(blank=True, null=True)
    pp = models.SmallIntegerField(blank=True, null=True)
    accuracy = models.SmallIntegerField(blank=True, null=True)
    priority = models.SmallIntegerField(blank=True, null=True)
    target = models.ForeignKey('MoveTargets', models.DO_NOTHING, related_name='movechangelog_target', blank=True, null=True)
    effect = models.ForeignKey('MoveEffects', models.DO_NOTHING, related_name='movechangelog_effect', blank=True, null=True)
    effect_chance = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'move_changelog'
        unique_together = (('move', 'changed_in_version_group'),)


class MoveDamageClassProse(models.Model):
    move_damage_class = models.ForeignKey('MoveDamageClasses', models.DO_NOTHING, related_name='movedamageclassprose_move_damage_class')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='movedamageclassprose_local_language')
    name = models.CharField(max_length=79, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'move_damage_class_prose'
        unique_together = (('move_damage_class', 'local_language'),)


class MoveDamageClasses(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'move_damage_classes'


class MoveEffectChangelog(models.Model):
    
    effect = models.ForeignKey('MoveEffects', models.DO_NOTHING, related_name='moveeffectchangelog_effect')
    changed_in_version_group = models.ForeignKey('VersionGroups', models.DO_NOTHING, related_name='moveeffectchangelog_changed_in_version_group')

    class Meta:
        managed = True
        db_table = 'move_effect_changelog'
        unique_together = (('effect', 'changed_in_version_group'),)


class MoveEffectChangelogProse(models.Model):
    move_effect_changelog = models.ForeignKey(MoveEffectChangelog, models.DO_NOTHING, related_name='moveeffectchangelogprose_move_effect_changelog')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='moveeffectchangelogprose_local_language')
    effect = models.TextField()

    class Meta:
        managed = True
        db_table = 'move_effect_changelog_prose'
        unique_together = (('move_effect_changelog', 'local_language'),)


class MoveEffectProse(models.Model):
    move_effect = models.ForeignKey('MoveEffects', models.DO_NOTHING, related_name='moveeffectprose_move_effect')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='moveeffectprose_local_language')
    short_effect = models.TextField(blank=True, null=True)
    effect = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'move_effect_prose'
        unique_together = (('move_effect', 'local_language'),)


class MoveEffects(models.Model):
    

    class Meta:
        managed = True
        db_table = 'move_effects'


class MoveFlagMap(models.Model):
    move = models.ForeignKey('Moves', models.DO_NOTHING, related_name='moveflagmap_move')
    move_flag = models.ForeignKey('MoveFlags', models.DO_NOTHING, related_name='moveflagmap_move_flag')

    class Meta:
        managed = True
        db_table = 'move_flag_map'
        unique_together = (('move', 'move_flag'),)


class MoveFlagProse(models.Model):
    move_flag = models.ForeignKey('MoveFlags', models.DO_NOTHING, related_name='moveflagprose_move_flag')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='moveflagprose_local_language')
    name = models.CharField(max_length=79, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'move_flag_prose'
        unique_together = (('move_flag', 'local_language'),)


class MoveFlags(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'move_flags'


class MoveFlavorSummaries(models.Model):
    move = models.ForeignKey('Moves', models.DO_NOTHING, related_name='moveflavorsummaries_move')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='moveflavorsummaries_local_language')
    flavor_summary = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'move_flavor_summaries'
        unique_together = (('move', 'local_language'),)


class MoveFlavorText(models.Model):
    move = models.ForeignKey('Moves', models.DO_NOTHING, related_name='moveflavortext_move')
    version_group = models.ForeignKey('VersionGroups', models.DO_NOTHING, related_name='moveflavortext_version_group')
    language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='moveflavortext_language')
    flavor_text = models.TextField()

    class Meta:
        managed = True
        db_table = 'move_flavor_text'
        unique_together = (('move', 'version_group', 'language'),)


class MoveMeta(models.Model):
    move = models.ForeignKey('Moves', models.DO_NOTHING, related_name="movemeta_move")
    meta_category = models.ForeignKey('MoveMetaCategories', models.DO_NOTHING, related_name='movemeta_meta_category')
    meta_ailment = models.ForeignKey('MoveMetaAilments', models.DO_NOTHING, related_name='movemeta_meta_ailment')
    min_hits = models.IntegerField(blank=True, null=True)
    max_hits = models.IntegerField(blank=True, null=True)
    min_turns = models.IntegerField(blank=True, null=True)
    max_turns = models.IntegerField(blank=True, null=True)
    drain = models.IntegerField()
    healing = models.IntegerField()
    crit_rate = models.IntegerField()
    ailment_chance = models.IntegerField()
    flinch_chance = models.IntegerField()
    stat_chance = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'move_meta'


class MoveMetaAilmentNames(models.Model):
    move_meta_ailment = models.ForeignKey('MoveMetaAilments', models.DO_NOTHING, related_name='movemetaailmentnames_move_meta_ailment')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='movemetaailmentnames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'move_meta_ailment_names'
        unique_together = (('move_meta_ailment', 'local_language'),)


class MoveMetaAilments(models.Model):
    
    identifier = models.CharField(unique=True, max_length=79)

    class Meta:
        managed = True
        db_table = 'move_meta_ailments'


class MoveMetaCategories(models.Model):
    
    identifier = models.CharField(unique=True, max_length=79)

    class Meta:
        managed = True
        db_table = 'move_meta_categories'


class MoveMetaCategoryProse(models.Model):
    move_meta_category = models.ForeignKey(MoveMetaCategories, models.DO_NOTHING, related_name='movemetacategoryprose_move_meta_category')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='movemetacategoryprose_local_language')
    description = models.TextField()

    class Meta:
        managed = True
        db_table = 'move_meta_category_prose'
        unique_together = (('move_meta_category', 'local_language'),)


class MoveMetaStatChanges(models.Model):
    move = models.ForeignKey('Moves', models.DO_NOTHING, related_name='movemetastatchanges_move')
    stat = models.ForeignKey('Stats', models.DO_NOTHING, related_name='movemetastatchanges_stat')
    change = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'move_meta_stat_changes'
        unique_together = (('move', 'stat'),)


class MoveNames(models.Model):
    move = models.ForeignKey('Moves', models.DO_NOTHING, related_name='movenames_move')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='movenames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'move_names'
        unique_together = (('move', 'local_language'),)


class MoveTargetProse(models.Model):
    move_target = models.ForeignKey('MoveTargets', models.DO_NOTHING, related_name='movetargetprose_move_target')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='movetargetprose_local_language')
    name = models.CharField(max_length=79, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'move_target_prose'
        unique_together = (('move_target', 'local_language'),)


class MoveTargets(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'move_targets'


class Moves(models.Model):
    
    identifier = models.CharField(max_length=79)
    generation = models.ForeignKey(Generations, models.DO_NOTHING, related_name='moves_generation')
    type = models.ForeignKey('Types', models.DO_NOTHING, related_name='moves_type')
    power = models.SmallIntegerField(blank=True, null=True)
    pp = models.SmallIntegerField(blank=True, null=True)
    accuracy = models.SmallIntegerField(blank=True, null=True)
    priority = models.SmallIntegerField()
    target = models.ForeignKey(MoveTargets, models.DO_NOTHING, related_name='moves_target')
    damage_class = models.ForeignKey(MoveDamageClasses, models.DO_NOTHING, related_name='moves_damage_class')
    effect = models.ForeignKey(MoveEffects, models.DO_NOTHING, related_name='moves_effect')
    effect_chance = models.IntegerField(blank=True, null=True)
    contest_type = models.ForeignKey(ContestTypes, models.DO_NOTHING, related_name='moves_contest_type', blank=True, null=True)
    contest_effect = models.ForeignKey(ContestEffects, models.DO_NOTHING, related_name='moves_contest_effect', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'moves'
        verbose_name_plural = 'moves'

    def __str__(self):
        return self.identifier

class NatureBattleStylePreferences(models.Model):
    nature = models.ForeignKey('Natures', models.DO_NOTHING, related_name='naturebattlestylepreferences_nature')
    move_battle_style = models.ForeignKey(MoveBattleStyles, models.DO_NOTHING, related_name='naturebattlestylepreferences_move_battle_style')
    low_hp_preference = models.IntegerField()
    high_hp_preference = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'nature_battle_style_preferences'
        unique_together = (('nature', 'move_battle_style'),)


class NatureNames(models.Model):
    nature = models.ForeignKey('Natures', models.DO_NOTHING, related_name='naturenames_nature')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='naturenames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'nature_names'
        unique_together = (('nature', 'local_language'),)


class Natures(models.Model):
    
    identifier = models.CharField(max_length=79)
    decreased_stat = models.ForeignKey('Stats', models.DO_NOTHING, related_name='natures_decreased_stat')
    increased_stat = models.ForeignKey('Stats', models.DO_NOTHING, related_name='natures_increased_stat')
    hates_flavor = models.ForeignKey(ContestTypes, models.DO_NOTHING, related_name='natures_hates_flavor')
    likes_flavor = models.ForeignKey(ContestTypes, models.DO_NOTHING, related_name='natures_likes_flavor')
    game_index = models.IntegerField(unique=True)

    class Meta:
        managed = True
        db_table = 'natures'


class PokedexVersionGroups(models.Model):
    pokedex = models.ForeignKey('Pokedexes', models.DO_NOTHING, related_name='pokedexversiongroups_pokedex')
    version_group = models.ForeignKey('VersionGroups', models.DO_NOTHING, related_name='pokedexversiongroups_version_group')

    class Meta:
        managed = True
        db_table = 'pokedex_version_groups'
        unique_together = (('pokedex', 'version_group'),)


class Pokedexes(models.Model):
    
    region = models.ForeignKey('Regions', models.DO_NOTHING, related_name='pokedexes_region', blank=True, null=True)
    identifier = models.CharField(max_length=79)
    is_main_series = models.BooleanField()

    class Meta:
        managed = True
        db_table = 'pokedexes'


class Pokemon(models.Model):
    
    identifier = models.CharField(max_length=79)
    species = models.ForeignKey('PokemonSpecies', models.DO_NOTHING, related_name='pokemon_species', blank=True, null=True)
    height = models.IntegerField()
    weight = models.IntegerField()
    base_experience = models.IntegerField()
    order = models.IntegerField()
    is_default = models.BooleanField()

    class Meta:
        managed = True
        db_table = 'pokemon'
        verbose_name_plural = 'pokemon'

    def __str__(self):
        return "#{}: {}".format(self.id, self.identifier)

class PokemonAbilities(models.Model):
    pokemon = models.ForeignKey(Pokemon, models.DO_NOTHING, related_name='pokemonabilities_pokemon', unique=False)
    ability = models.ForeignKey(Abilities, models.DO_NOTHING, related_name='pokemonabilities_ability')
    is_hidden = models.BooleanField()
    slot = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'pokemon_abilities'
        unique_together = (('pokemon', 'slot'),)


class PokemonColorNames(models.Model):
    pokemon_color = models.ForeignKey('PokemonColors', models.DO_NOTHING, related_name='pokemoncolornames_pokemon_color')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='pokemoncolornames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'pokemon_color_names'
        unique_together = (('pokemon_color', 'local_language'),)


class PokemonColors(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'pokemon_colors'


class PokemonDexNumbers(models.Model):
    species = models.ForeignKey('PokemonSpecies', models.DO_NOTHING, related_name='pokemondexnumbers_species')
    pokedex = models.ForeignKey(Pokedexes, models.DO_NOTHING, related_name='pokemondexnumbers_pokedex')
    pokedex_number = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'pokemon_dex_numbers'
        unique_together = (('species', 'pokedex'),)


class PokemonEggGroups(models.Model):
    species = models.ForeignKey('PokemonSpecies', models.DO_NOTHING, related_name='pokemonegggroups_species')
    egg_group = models.ForeignKey(EggGroups, models.DO_NOTHING, related_name='pokemonegggroups_egg_group')
    class Meta:
        managed = True
        db_table = 'pokemon_egg_groups'
        unique_together = (('species', 'egg_group'),)


class PokemonEvolution(models.Model):
    
    evolved_species = models.ForeignKey('PokemonSpecies', models.DO_NOTHING, related_name='pokemonevolution_evolved_species')
    evolution_trigger = models.ForeignKey(EvolutionTriggers, models.DO_NOTHING, related_name='pokemonevolution_evolution_trigger')
    trigger_item = models.ForeignKey(Items, models.DO_NOTHING, related_name='pokemonevolution_trigger_item', blank=True, null=True)
    minimum_level = models.IntegerField(blank=True, null=True)
    gender = models.ForeignKey(Genders, models.DO_NOTHING, related_name='pokemonevolution_gender', blank=True, null=True)
    location = models.ForeignKey(Locations, models.DO_NOTHING, related_name='pokemonevolution_location', blank=True, null=True)
    held_item = models.ForeignKey(Items, models.DO_NOTHING, related_name='pokemonevolution_held_item', blank=True, null=True)
    time_of_day = models.CharField(max_length=5, blank=True, null=True)
    known_move = models.ForeignKey(Moves, models.DO_NOTHING, related_name='pokemonevolution_known_move', blank=True, null=True)
    known_move_type = models.ForeignKey('Types', models.DO_NOTHING, related_name='pokemonevolution_known_move_type', blank=True, null=True)
    minimum_happiness = models.IntegerField(blank=True, null=True)
    minimum_beauty = models.IntegerField(blank=True, null=True)
    minimum_affection = models.IntegerField(blank=True, null=True)
    relative_physical_stats = models.IntegerField(blank=True, null=True)
    party_species = models.ForeignKey('PokemonSpecies', models.DO_NOTHING, related_name='pokemonevolution_party_species', blank=True, null=True)
    party_type = models.ForeignKey('Types', models.DO_NOTHING, related_name='pokemonevolution_party_type', blank=True, null=True)
    trade_species = models.ForeignKey('PokemonSpecies', models.DO_NOTHING, related_name='pokemonevolution_trade_species', blank=True, null=True)
    needs_overworld_rain = models.BooleanField()
    turn_upside_down = models.BooleanField()

    class Meta:
        managed = True
        db_table = 'pokemon_evolution'


class PokemonFormGenerations(models.Model):
    pokemon_form = models.ForeignKey('PokemonForms', models.DO_NOTHING, related_name='pokemonformgenerations_pokemon_form')
    generation = models.ForeignKey(Generations, models.DO_NOTHING, related_name='pokemonformgenerations_generation')
    game_index = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'pokemon_form_generations'
        unique_together = (('pokemon_form', 'generation'),)


class PokemonFormNames(models.Model):
    pokemon_form = models.ForeignKey('PokemonForms', models.DO_NOTHING, related_name='pokemonformnames_pokemon_form')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='pokemonformnames_local_language')
    form_name = models.CharField(max_length=79, blank=True, null=True)
    pokemon_name = models.CharField(max_length=79, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'pokemon_form_names'
        unique_together = (('pokemon_form', 'local_language'),)


class PokemonForms(models.Model):
    
    identifier = models.CharField(max_length=79)
    form_identifier = models.CharField(max_length=79, blank=True, null=True)
    pokemon = models.ForeignKey(Pokemon, models.DO_NOTHING, related_name='pokemonforms_pokemon')
    introduced_in_version_group = models.ForeignKey('VersionGroups', models.DO_NOTHING, related_name='pokemonforms_introduced_in_version_group', blank=True, null=True)
    is_default = models.BooleanField()
    is_battle_only = models.BooleanField()
    is_mega = models.BooleanField()
    form_order = models.IntegerField()
    order = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'pokemon_forms'


class PokemonGameIndices(models.Model):
    pokemon = models.ForeignKey(Pokemon, models.DO_NOTHING, related_name='pokemongameindices_pokemon')
    version = models.ForeignKey('Versions', models.DO_NOTHING, related_name='pokemongameindices_version')
    game_index = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'pokemon_game_indices'
        unique_together = (('pokemon', 'version'),)


class PokemonGenderRatios(models.Model):
    
    pokemon = models.ForeignKey(Pokemon, models.DO_NOTHING, related_name='pokemongenderratios_pokemon', blank=True, null=True)
    ratio = models.ForeignKey(GenderRatios, models.DO_NOTHING, related_name='pokemongenderratios_ratio', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'pokemon_gender_ratios'


class PokemonHabitatNames(models.Model):
    pokemon_habitat = models.ForeignKey('PokemonHabitats', models.DO_NOTHING, related_name='pokemonhabitatnames_pokemon_habitat')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='pokemonhabitatnames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'pokemon_habitat_names'
        unique_together = (('pokemon_habitat', 'local_language'),)


class PokemonHabitats(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'pokemon_habitats'


class PokemonItems(models.Model):
    pokemon = models.ForeignKey(Pokemon, models.DO_NOTHING, related_name='pokemonitems_pokemon')
    version = models.ForeignKey('Versions', models.DO_NOTHING, related_name='pokemonitems_version')
    item = models.ForeignKey(Items, models.DO_NOTHING, related_name='pokemonitems_item')
    rarity = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'pokemon_items'
        unique_together = (('pokemon', 'version', 'item'),)


class PokemonMoveMethodProse(models.Model):
    pokemon_move_method = models.ForeignKey('PokemonMoveMethods', models.DO_NOTHING, related_name='pokemonmovemethodprose_pokemon_move_type')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='pokemonmovemethodprose_local_language')
    name = models.CharField(max_length=79, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'pokemon_move_method_prose'
        unique_together = (('pokemon_move_method', 'local_language'),)


class PokemonMoveMethods(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'pokemon_move_methods'


class PokemonMoves(models.Model):
    pokemon = models.ForeignKey(Pokemon, models.DO_NOTHING, related_name='pokemonmoves_pokemon')
    version_group = models.ForeignKey('VersionGroups', models.DO_NOTHING, related_name='pokemonmoves_version_group')
    move = models.ForeignKey(Moves, models.DO_NOTHING, related_name='pokemonmoves_move')
    pokemon_move_method = models.ForeignKey(PokemonMoveMethods, models.DO_NOTHING, related_name='pokemonmoves_pokemon_move_method')
    level = models.IntegerField()
    order = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'pokemon_moves'
        unique_together = (('pokemon', 'version_group', 'move', 'pokemon_move_method', 'level'),)


class PokemonProse(models.Model):
    
    pokemon = models.ForeignKey(Pokemon, models.DO_NOTHING, related_name='pokemonprose_pokemon', blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'pokemon_prose'


class PokemonShapeProse(models.Model):
    pokemon_shape = models.ForeignKey('PokemonShapes', models.DO_NOTHING, related_name='pokemonshapeprose_pokemon_shape')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='pokemonshapeprose_local_language')
    name = models.CharField(max_length=79, blank=True, null=True)
    awesome_name = models.CharField(max_length=79, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'pokemon_shape_prose'
        unique_together = (('pokemon_shape', 'local_language'),)


class PokemonShapes(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'pokemon_shapes'


class PokemonSpecies(models.Model):
    
    identifier = models.CharField(max_length=79)
    generation = models.ForeignKey(Generations, models.DO_NOTHING, related_name='pokemonspecies_generation', blank=True, null=True)
    evolves_from_species = models.ForeignKey('self', models.DO_NOTHING, related_name='pokemonspecies_evolves_from_species', blank=True, null=True)
    evolution_chain = models.ForeignKey(EvolutionChains, models.DO_NOTHING, related_name='pokemonspecies_evolution_chain', blank=True, null=True)
    color = models.ForeignKey(PokemonColors, models.DO_NOTHING, related_name='pokemonspecies_color')
    shape = models.ForeignKey(PokemonShapes, models.DO_NOTHING, related_name='pokemonspecies_shape')
    habitat = models.ForeignKey(PokemonHabitats, models.DO_NOTHING, related_name='pokemonspecies_habitit', blank=True, null=True)
    gender_rate = models.IntegerField()
    capture_rate = models.IntegerField()
    base_happiness = models.IntegerField()
    is_baby = models.BooleanField()
    hatch_counter = models.IntegerField()
    has_gender_differences = models.BooleanField()
    growth_rate = models.ForeignKey(GrowthRates, models.DO_NOTHING, related_name='pokemonspecies_growth_rate')
    forms_switchable = models.BooleanField()
    order = models.IntegerField()
    conquest_order = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'pokemon_species'

class PokemonSpeciesFlavorSummaries(models.Model):
    pokemon_species = models.ForeignKey(PokemonSpecies, models.DO_NOTHING, related_name='pokemonspeciesflavorsummaries_pokemon_species')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='pokemonspeciesflavorsummaries_local_language')
    flavor_summary = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'pokemon_species_flavor_summaries'
        unique_together = (('pokemon_species', 'local_language'),)


class PokemonSpeciesFlavorText(models.Model):
    species = models.ForeignKey(PokemonSpecies, models.DO_NOTHING, related_name='pokemonspeciesflavortext_species')
    version = models.ForeignKey('Versions', models.DO_NOTHING, related_name='pokemonspeciesflavortext_version')
    language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='pokemonspeciesflavortext_language')
    flavor_text = models.TextField()

    class Meta:
        managed = True
        db_table = 'pokemon_species_flavor_text'
        unique_together = (('species', 'version', 'language'),)


class PokemonSpeciesNames(models.Model):
    pokemon_species = models.ForeignKey(PokemonSpecies, models.DO_NOTHING, related_name='pokemonspeciesnames_pokemon_species')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='pokemonspeciesnames_local_language')
    name = models.CharField(max_length=79, blank=True, null=True)
    genus = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'pokemon_species_names'
        unique_together = (('pokemon_species', 'local_language'),)


class PokemonSpeciesProse(models.Model):
    pokemon_species = models.ForeignKey(PokemonSpecies, models.DO_NOTHING, related_name='pokemonspeciesprose_pokemon_species')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='pokemonspeciesprose_local_language')
    form_description = models.TextField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'pokemon_species_prose'
        unique_together = (('pokemon_species', 'local_language'),)


class PokemonStats(models.Model):
    pokemon = models.ForeignKey(Pokemon, models.DO_NOTHING, related_name='pokemonstats_pokemon')
    stat = models.ForeignKey('Stats', models.DO_NOTHING, related_name='pokemonstats_stat')
    base_stat = models.IntegerField()
    effort = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'pokemon_stats'
        unique_together = (('pokemon', 'stat'),)


class PokemonTypes(models.Model):
    pokemon = models.ForeignKey(Pokemon, models.DO_NOTHING, related_name='pokemontypes_pokemon')
    type = models.ForeignKey('Types', models.DO_NOTHING, related_name='pokemontypes_type')
    slot = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'pokemon_types'
        unique_together = (('pokemon', 'slot'),)
    
    def __str__(self):
        return self.type

class RegionNames(models.Model):
    region = models.ForeignKey('Regions', models.DO_NOTHING, related_name='regionnames_region')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='regionnames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'region_names'
        unique_together = (('region', 'local_language'),)


class Regions(models.Model):
    
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'regions'
        verbose_name_plural = 'regions'

    def __str__(self):
        return self.identifier


class StatNames(models.Model):
    stat = models.ForeignKey('Stats', models.DO_NOTHING, related_name='statnames_stat')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='statnames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'stat_names'
        unique_together = (('stat', 'local_language'),)


class Stats(models.Model):
    
    damage_class = models.ForeignKey(MoveDamageClasses, models.DO_NOTHING, related_name='stats_damage_class', blank=True, null=True)
    identifier = models.CharField(max_length=79)
    is_battle_only = models.BooleanField()
    game_index = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'stats'
        verbose_name_plural = 'stats'

    def __str__(self):
        return self.identifier

class TypeEfficacy(models.Model):
    damage_type = models.ForeignKey('Types', models.DO_NOTHING, related_name='typeefficacy_damage_type')
    target_type = models.ForeignKey('Types', models.DO_NOTHING, related_name='typeefficacy_target_type')
    damage_factor = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'type_efficacy'
        unique_together = (('damage_type', 'target_type'),)


class TypeGameIndices(models.Model):
    type = models.ForeignKey('Types', models.DO_NOTHING, related_name='typegameindices_type')
    generation = models.ForeignKey(Generations, models.DO_NOTHING, related_name='typegameindices_generation')
    game_index = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'type_game_indices'
        unique_together = (('type', 'generation'),)


class TypeNames(models.Model):
    type = models.ForeignKey('Types', models.DO_NOTHING, related_name='typenames_type')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='typenames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'type_names'
        unique_together = (('type', 'local_language'),)


class Types(models.Model):
    
    identifier = models.CharField(max_length=79)
    generation = models.ForeignKey(Generations, models.DO_NOTHING, related_name='types_generation')
    damage_class = models.ForeignKey(MoveDamageClasses, models.DO_NOTHING, related_name='types_damage_class', blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'types'
        verbose_name_plural = 'types'

    def __str__(self):
        return self.identifier


class VersionGroupPokemonMoveMethods(models.Model):
    version_group = models.ForeignKey('VersionGroups', models.DO_NOTHING, related_name='versiongrouppokemonmovemethods_version_group')
    pokemon_move_method = models.ForeignKey(PokemonMoveMethods, models.DO_NOTHING, related_name='versiongrouppokemonmovemethods_pokemon_move_method')

    class Meta:
        managed = True
        db_table = 'version_group_pokemon_move_methods'
        unique_together = (('version_group', 'pokemon_move_method'),)


class VersionGroupRegions(models.Model):
    version_group = models.ForeignKey('VersionGroups', models.DO_NOTHING, related_name='versiongroupregions_version_group')
    region = models.ForeignKey(Regions, models.DO_NOTHING, related_name='versiongroupregions_region')

    class Meta:
        managed = True
        db_table = 'version_group_regions'
        unique_together = (('version_group', 'region'),)


class VersionGroups(models.Model):
    
    identifier = models.CharField(unique=True, max_length=79)
    generation = models.ForeignKey(Generations, models.DO_NOTHING, related_name='versiongroups_generation')
    order = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'version_groups'


class VersionNames(models.Model):
    version = models.ForeignKey('Versions', models.DO_NOTHING, related_name='versionnames_version')
    local_language = models.ForeignKey(Languages, models.DO_NOTHING, related_name='versionnames_local_language')
    name = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'version_names'
        unique_together = (('version', 'local_language'),)


class Versions(models.Model):
    
    version_group = models.ForeignKey(VersionGroups, models.DO_NOTHING, related_name='versions_version_group')
    identifier = models.CharField(max_length=79)

    class Meta:
        managed = True
        db_table = 'versions'
