FROM python:3.6

LABEL Author="Zach Bresser"
LABEL Email="admin@chbresser.com"
LABEL Description="Dockerfile for CHBresser website"

ENV SECRET_KEY="DOCKER_TESTING_SECRET_KEY"

COPY . /chbresser
EXPOSE 8000

RUN ls /chbresser && python3 -m pip install -r /chbresser/requirements.txt

ENTRYPOINT ["python", "/chbresser/manage.py"]
CMD ["runserver", "0.0.0.0:8000"]
